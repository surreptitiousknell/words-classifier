package eu.proszowski.surreptitiousknell.wordsclassifier;

public enum WordType {
    KNOWN,
    UNKNOWN,
    LEARNING_IN_PROGRESS
}
