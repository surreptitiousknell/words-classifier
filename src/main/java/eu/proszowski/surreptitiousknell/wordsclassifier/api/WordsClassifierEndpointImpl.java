package eu.proszowski.surreptitiousknell.wordsclassifier.api;

import eu.proszowski.surreptitiousknell.wordsclassifier.UserId;
import eu.proszowski.surreptitiousknell.wordsclassifier.Word;
import eu.proszowski.surreptitiousknell.wordsclassifier.WordId;
import eu.proszowski.surreptitiousknell.wordsclassifier.WordsClassifierFacade;
import org.springframework.stereotype.Component;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
class WordsClassifierEndpointImpl implements WordsClassifierEndpoint {

    private final WordsClassifierFacade wordsClassifierFacade;

    WordsClassifierEndpointImpl(final WordsClassifierFacade wordsClassifierFacade) {
        this.wordsClassifierFacade = wordsClassifierFacade;
    }

    @Override
    public Set<WordDto> getAllWordsForUser(final UUID userId) {
        return wordsClassifierFacade.getWordsForUser(UserId.builder().raw(userId).build())
                .stream()
                .map(WordDto::fromModel)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<WordDto> classifyWords(final UUID userId, final Set<UUID> wordIds) {
        final Set<WordId> modelWordIds = wordIds.stream()
                .map(id -> WordId.builder().raw(id).build())
                .collect(Collectors.toSet());

        return wordsClassifierFacade.classifyWords(UserId.builder().raw(userId).build(), modelWordIds)
                .stream()
                .map(WordDto::fromModel)
                .collect(Collectors.toSet());
    }

    @Override
    public void markWords(final UUID userId, final Set<WordDto> wordDtos) {
        final Set<Word> words = wordDtos.stream()
                .map(word -> Word.builder().wordId(WordId.builder().raw(word.getWordId()).build()).wordType(word.getWordType()).build())
                .collect(Collectors.toSet());

        final UserId modelUserId = UserId.builder().raw(userId).build();

        words.forEach(word -> wordsClassifierFacade.markWord(modelUserId, word, word.getWordType()));
    }
}
