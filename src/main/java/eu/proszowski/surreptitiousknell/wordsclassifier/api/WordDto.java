package eu.proszowski.surreptitiousknell.wordsclassifier.api;

import eu.proszowski.surreptitiousknell.wordsclassifier.Word;
import eu.proszowski.surreptitiousknell.wordsclassifier.WordId;
import eu.proszowski.surreptitiousknell.wordsclassifier.WordType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
class WordDto {

    @NonNull
    private UUID wordId;
    @NonNull
    private WordType wordType;

    static WordDto fromModel(final Word word){
        return new WordDto(word.getWordId().getRaw(), word.getWordType());
    }

    static Word toModel(final WordDto word){
        return Word.builder().wordType(word.getWordType()).wordId(WordId.builder().raw(word.getWordId()).build()).build();
    }
}
