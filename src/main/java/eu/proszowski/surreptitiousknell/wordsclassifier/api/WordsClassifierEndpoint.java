package eu.proszowski.surreptitiousknell.wordsclassifier.api;

import eu.proszowski.surreptitiousknell.wordsclassifier.UserId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.Set;
import java.util.UUID;

@RestController
interface WordsClassifierEndpoint {

    @GetMapping("getAllWordsForUserId/{userId}")
    Set<WordDto> getAllWordsForUser(@PathVariable UUID userId);

    @PostMapping("classifyWordsForUserId/{userId}")
    Set<WordDto> classifyWords(@PathVariable UUID userId, @RequestBody  Set<UUID> wordIds);

    @PostMapping("markWordsForUserId/{userId}")
    void markWords(@PathVariable UUID userId, @RequestBody Set<WordDto> wordDtos);
}
