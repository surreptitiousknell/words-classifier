package eu.proszowski.surreptitiousknell.wordsclassifier;

import lombok.Builder;
import java.util.Set;
import java.util.stream.Collectors;

@Builder
public class WordsClassifierFacade {
    private final WordsRepository wordsRepository;

    public Set<Word> classifyWords(final UserId userId, final Set<WordId> wordIds) {
        return wordIds.stream()
                .map(wordId -> wordsRepository.findById(userId, wordId)
                        .orElseGet(() -> wordsRepository.saveUnknownWord(userId, wordId))
                )
                .collect(Collectors.toSet());
    }

    public Set<Word> getWordsForUser(final UserId userId) {
        return wordsRepository.findAllByUserId(userId);
    }

    public void markWord(final UserId userId, final Word word, final WordType wordType){
        final Word updatedWord = Word.builder().wordId(word.getWordId()).wordType(wordType).build();
        wordsRepository.save(userId, updatedWord);
    }
}
