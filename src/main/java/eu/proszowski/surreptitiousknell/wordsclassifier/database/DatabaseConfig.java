package eu.proszowski.surreptitiousknell.wordsclassifier.database;

import eu.proszowski.surreptitiousknell.wordsclassifier.WordsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseConfig {
    @Bean
    WordsRepository wordsRepository(final PostgreSQLWordsRepository postgreSQLWordsRepository){
        return new PostgresSQLWordsDatabase(postgreSQLWordsRepository);
    }
}
