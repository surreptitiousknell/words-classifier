package eu.proszowski.surreptitiousknell.wordsclassifier.database;

import eu.proszowski.surreptitiousknell.wordsclassifier.WordType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
class WordEntity {

    @Id
    private UUID id;
    private UUID userId;
    private WordType wordType;
}
