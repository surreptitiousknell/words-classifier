package eu.proszowski.surreptitiousknell.wordsclassifier.database;

import org.springframework.data.repository.CrudRepository;
import java.util.Set;
import java.util.UUID;

public interface PostgreSQLWordsRepository extends CrudRepository<WordEntity, UUID> {
    Set<WordEntity> findAllByUserId(UUID raw);
}
