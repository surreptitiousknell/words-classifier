package eu.proszowski.surreptitiousknell.wordsclassifier.database;

class WordMapperException extends RuntimeException{

    public WordMapperException(){
        super("Error occured during mapping from entity to model");
    }

}
