package eu.proszowski.surreptitiousknell.wordsclassifier.database;

import eu.proszowski.surreptitiousknell.wordsclassifier.UserId;
import eu.proszowski.surreptitiousknell.wordsclassifier.Word;
import eu.proszowski.surreptitiousknell.wordsclassifier.WordId;
import eu.proszowski.surreptitiousknell.wordsclassifier.WordType;
import eu.proszowski.surreptitiousknell.wordsclassifier.WordsRepository;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class PostgresSQLWordsDatabase implements WordsRepository {

    private final PostgreSQLWordsRepository postgreSQLWordsRepository;

    PostgresSQLWordsDatabase(final PostgreSQLWordsRepository postgreSQLWordsRepository) {
        this.postgreSQLWordsRepository = postgreSQLWordsRepository;
    }

    @Override
    public Optional<Word> findById(final UserId userId, final WordId wordId) {
        return postgreSQLWordsRepository.findById(wordId.getRaw())
                .map(this::mapToModel);
    }

    @Override
    public Word saveUnknownWord(final UserId userId, final WordId wordId) {
        final WordEntity wordEntity = new WordEntity(wordId.getRaw(), userId.getRaw(), WordType.UNKNOWN);
        final WordEntity savedEntity = postgreSQLWordsRepository.save(wordEntity);
        return mapToModel(savedEntity);
    }

    @Override
    public void saveAll(final UserId userId, final Set<Word> words) {
        final Set<WordEntity> wordEntities = words.stream().map(word -> new WordEntity(word.getWordId().getRaw(), userId.getRaw(), word.getWordType())).collect(Collectors.toSet());
        postgreSQLWordsRepository.saveAll(wordEntities);
    }

    @Override
    public void save(final UserId userId, final Word word) {
        final WordEntity wordEntity = new WordEntity(word.getWordId().getRaw(), userId.getRaw(), word.getWordType());
        postgreSQLWordsRepository.save(wordEntity);
    }

    @Override
    public Set<Word> findAllByUserId(final UserId userId) {
        return postgreSQLWordsRepository.findAllByUserId(userId.getRaw())
                .stream()
                .map(this::mapToModel)
                .collect(Collectors.toSet());
    }

    private Word mapToModel(final WordEntity wordEntity) {
        return Word.builder()
                .wordId(WordId.builder().raw(wordEntity.getId()).build())
                .wordType(wordEntity.getWordType())
                .build();
    }
}
