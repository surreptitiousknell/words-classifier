package eu.proszowski.surreptitiousknell.wordsclassifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class WordsClassifierApplication {

	@Autowired
	private ApplicationContext context;


	public static void main(String[] args) {
		SpringApplication.run(WordsClassifierApplication.class, args);
	}
}
