package eu.proszowski.surreptitiousknell.wordsclassifier;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class Word {
    private WordId wordId;
    private WordType wordType;
}
