package eu.proszowski.surreptitiousknell.wordsclassifier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class AppConfig {

    @Bean
    WordsClassifierFacade wordsClassifierFacade(final WordsRepository wordsRepository){
        return new WordsClassifierFacade(wordsRepository);
    }
}
