package eu.proszowski.surreptitiousknell.wordsclassifier;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import java.util.UUID;

@Builder
@Value
public class UserId {
    @NonNull
    private UUID raw;
}
