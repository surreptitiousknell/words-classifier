package eu.proszowski.surreptitiousknell.wordsclassifier;

import java.util.Optional;
import java.util.Set;

public interface WordsRepository {
    Optional<Word> findById(UserId userId, WordId wordId);

    void save(UserId userId, Word word);

    Set<Word> findAllByUserId(UserId userId);

    Word saveUnknownWord(final UserId userId, final WordId wordId);

    void saveAll(UserId userId, Set<Word> words);
}
