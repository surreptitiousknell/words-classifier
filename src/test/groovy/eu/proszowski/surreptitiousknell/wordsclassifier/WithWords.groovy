package eu.proszowski.surreptitiousknell.wordsclassifier


trait WithWords {

    Set<Word> getFewWords() {

        return [
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.KNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.LEARNING_IN_PROGRESS).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.KNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.LEARNING_IN_PROGRESS).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.KNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.LEARNING_IN_PROGRESS).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.KNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.LEARNING_IN_PROGRESS).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.KNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.LEARNING_IN_PROGRESS).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.KNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.LEARNING_IN_PROGRESS).build(),
        ]
    }

    Set<Word> getSomeUnknownWords() {

        return [
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
                Word.builder().wordId(WordId.builder().raw(UUID.randomUUID()).build()).wordType(WordType.UNKNOWN).build(),
        ]
    }
}