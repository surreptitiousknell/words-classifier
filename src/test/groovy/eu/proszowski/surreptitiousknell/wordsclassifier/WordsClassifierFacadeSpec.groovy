package eu.proszowski.surreptitiousknell.wordsclassifier

import spock.lang.Specification


class WordsClassifierFacadeSpec extends Specification implements WithSetup, WithWords {

    void "should properly classify words for given user id"(){
        given:
        UserId userId = UserId.builder().raw(UUID.randomUUID()).build()
        Set<Word> expectedWords = getFewWords();
        Set<WordId> wordIds = expectedWords.collect {
            it.wordId
        }

        thereIsAnUserWithAssociatedWords(userId, expectedWords)

        when:
        Set<Word> actualWords = wordsClassifierFacade.classifyWords(userId, wordIds)

        then:
        actualWords == expectedWords
    }

    void "should classify unknown words and save them to database"(){
        given:
        UserId userId = UserId.builder().raw(UUID.randomUUID()).build()
        Set<Word> expectedWords = getSomeUnknownWords();
        Set<WordId> wordIds = expectedWords.collect {
            it.wordId
        }

        thereIsAnUserWithoutAssociatedWords(userId)

        when:
        wordsClassifierFacade.classifyWords(userId, wordIds)
        Set<Word> actualWords = wordsClassifierFacade.getWordsForUser(userId)

        then:
        actualWords == expectedWords
    }

    void "should change types of words"(){
        given:
        UserId userId = UserId.builder().raw(UUID.randomUUID()).build()
        WordId wordId = WordId.builder().raw(UUID.randomUUID()).build()
        Word word = Word.builder().wordId(wordId).wordType(WordType.UNKNOWN).build()
        WordType expectedWordType = WordType.LEARNING_IN_PROGRESS

        thereIsAnUserWithAssociatedWords(userId, [word] as Set)

        when:
        wordsClassifierFacade.markWord(userId, word, expectedWordType)
        Word actualWord = wordsClassifierFacade.getWordsForUser(userId).find{ it.wordId == wordId}

        then:
        actualWord.wordType == expectedWordType
    }

}
