package eu.proszowski.surreptitiousknell.wordsclassifier


trait WithSetup {
    WordsClassifierFacade wordsClassifierFacade;

    void thereIsAnUserWithAssociatedWords(UserId userId, Set<Word> words) {
        FakeWordsRepository wordsRepository = FakeWordsRepository.with(userId, words)
        wordsClassifierFacade = WordsClassifierFacade.builder().wordsRepository(wordsRepository).build()
    }

    void thereIsAnUserWithoutAssociatedWords(UserId userId) {
        FakeWordsRepository wordsRepository = new FakeWordsRepository(usersWithWords: [(userId) : [] as Set])
        wordsClassifierFacade = WordsClassifierFacade.builder().wordsRepository(wordsRepository).build()
    }
}