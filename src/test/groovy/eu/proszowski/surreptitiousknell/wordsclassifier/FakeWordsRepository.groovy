package eu.proszowski.surreptitiousknell.wordsclassifier

import groovy.transform.MapConstructor


@MapConstructor
class FakeWordsRepository implements WordsRepository {

    Map<UserId, Set<Word>> usersWithWords

    static FakeWordsRepository with(UserId userId, Set<Word> words){
        Map<UserId, Set<Word>> usersWithWords = [:]
        usersWithWords.put(userId, words)
        return new FakeWordsRepository(usersWithWords: usersWithWords)
    }

    @Override
    Optional<Word> findById(final UserId userId, final WordId wordId) {
        return Optional.ofNullable(usersWithWords.get(userId).find{
            it.wordId == wordId
        })
    }

    @Override
    void save(final UserId userId, final Word word) {
        Set<Word> words = usersWithWords.get(userId)
        words.removeIf{
            it.wordId == word.wordId
        }
        usersWithWords.put(userId, words)
        usersWithWords.merge(userId, [word] as Set, { prev, singleton -> prev + singleton})
    }

    @Override
    Set<Word> findAllByUserId(final UserId userId) {
        return usersWithWords.get(userId)
    }

    @Override
    Word saveUnknownWord(final UserId userId, final WordId wordId) {
        Word word = Word.builder().wordType(WordType.UNKNOWN).wordId(wordId).build()
        save(userId, word)
        return word
    }

    @Override
    void saveAll(final UserId userId, final Set<Word> words) {
        words.each { save(userId, it) }
    }
}
