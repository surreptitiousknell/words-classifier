package eu.proszowski.surreptitiousknell.wordsclassifier.database

import eu.proszowski.surreptitiousknell.wordsclassifier.UserId
import eu.proszowski.surreptitiousknell.wordsclassifier.WithWords
import eu.proszowski.surreptitiousknell.wordsclassifier.Word
import eu.proszowski.surreptitiousknell.wordsclassifier.WordId
import eu.proszowski.surreptitiousknell.wordsclassifier.WordType
import eu.proszowski.surreptitiousknell.wordsclassifier.WordsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification


@SpringBootTest
class PostgresSQLWordsDatabaseTest extends Specification implements WithWords {

    @Autowired
    WordsRepository wordsRepository;

    void "should retrieve word entity after saving it"(){
        given:
        UserId userId = UserId.builder().raw(UUID.randomUUID()).build()
        WordId wordId = WordId.builder().raw(UUID.randomUUID()).build()
        WordType wordType = WordType.UNKNOWN
        WordEntity wordEntity = new WordEntity(userId.raw, wordId.raw, wordType)
        Word expectedWord = Word.builder().wordId(wordId).wordType(wordType).build()

        when:
        wordsRepository.save(userId, expectedWord)
        Word actualWord = wordsRepository.findById(userId, wordId).get()

        then:
        actualWord == expectedWord
    }

    void "should retrieve all word entities for given userId"(){
        given:
        UserId userId = UserId.builder().raw(UUID.randomUUID()).build()
        Set<Word> expectedWords = getFewWords()

        when:
        wordsRepository.saveAll(userId, expectedWords)
        Set<Word> actualWords = wordsRepository.findAllByUserId(userId)

        then:
        actualWords == expectedWords
    }

    void "should update word"(){
        given:
        UserId userId = UserId.builder().raw(UUID.randomUUID()).build()
        WordId wordId = WordId.builder().raw(UUID.randomUUID()).build()
        Word word = Word.builder().wordId(wordId).wordType(WordType.UNKNOWN).build()

        when:
        wordsRepository.save(userId, word)
        word = word.toBuilder().wordType(WordType.KNOWN).build()
        wordsRepository.save(userId, word)
        Set<Word> actualWords = wordsRepository.findAllByUserId(userId)

        then:
        actualWords.size() == 1
        actualWords.find{ it.wordId == wordId } == word
    }
}
